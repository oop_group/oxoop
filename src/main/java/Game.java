
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kitti
 */
public class Game {

    private Player O;
    private Player X;
    private int row;
    private int col;
    private Board board;

    Scanner kb = new Scanner(System.in);

    public Game() {
        this.O = new Player('O');
        this.X = new Player('X');
    }

    public void newBoard() {
        this.board = new Board(O, X);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX game!!!");
    }

    public void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void showTable() {
        char[][] table = this.board.getTable();
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row,col");
            row = kb.nextInt();
            col = kb.nextInt();
            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }

    public boolean isFinish() {
        if (board.isDraw() || board.isWin()) {
            return true;
        }
        return false;
    }

    public void showStat() {
        System.out.println(O);
        System.out.println(X);
    }

    public void showResult() {
        if (board.isDraw()) {
            System.out.println("Draw!!!");
        } else if (board.isWin()) {
            System.out.println(board.getCurrentPlayer().getSymbol() + "Win");
        }
    }
}
